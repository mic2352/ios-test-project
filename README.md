
# Readme

This is the readme file for "test weather app" project

Conception process is done in a slide, which can be found inside Google Drive : [link to view the slide](https://docs.google.com/presentation/d/1AMX5uN5RovTvOSslNXohU1LpAjBOT3RPY53cTsQO-Ik/edit?usp=sharing)

I believe to have achieved all requirements.

## Project phase

- Friday night: conception in slides
- Saturday night: UI, API implementation
- Sunday afternoon: file read/load, network check & error handling
- Sunday night: refactor MVC to MVP, GPS, demo tests

## About Git structure

- Each development is conducted in feature branch and then merge to master.
- Each commit in master is fully runnable, considered as continuous delivery

## ToDos

- Write some comments, since it barely has any for now
- Write more test cases, since I don't have much time in the end
