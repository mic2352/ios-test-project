//
//  Test_Weather_AppTests.swift
//  Test Weather AppTests
//
//  Created by chenxu on 2019/10/18.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest
@testable import Test_Weather_App

class Test_Weather_AppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testApiDecoding() {
        let jsonString = """
{"request_state":200,"request_key":"fd543c77e33d6c8a5e218e948a19e487","message":"OK","model_run":"02","source":"internal:GFS:1","2019-10-18 05:00:00":{"temperature":{"2m":287.8,"sol":288.1,"500hPa":-0.1,"850hPa":-0.1},"pression":{"niveau_de_la_mer":100670},"pluie":0.3,"pluie_convective":0.3,"humidite":{"2m":75},"vent_moyen":{"10m":22.7},"vent_rafales":{"10m":46.9},"vent_direction":{"10m":198},"iso_zero":2225,"risque_neige":"non","cape":0,"nebulosite":{"haute":100,"moyenne":100,"basse":56,"totale":100}},"2019-10-18 08:00:00":{"temperature":{"2m":287.6,"sol":287.9,"500hPa":-0.1,"850hPa":-0.1},"pression":{"niveau_de_la_mer":100620},"pluie":1.3,"pluie_convective":1.3,"humidite":{"2m":70.2},"vent_moyen":{"10m":30.7},"vent_rafales":{"10m":57.8},"vent_direction":{"10m":222},"iso_zero":2115,"risque_neige":"non","cape":0,"nebulosite":{"haute":50,"moyenne":50,"basse":75,"totale":96}},"2019-10-18 11:00:00":{"temperature":{"2m":287.2,"sol":287.2,"500hPa":-0.1,"850hPa":-0.1},"pression":{"niveau_de_la_mer":100840},"pluie":0.6,"pluie_convective":0.4,"humidite":{"2m":60},"vent_moyen":{"10m":25.6},"vent_rafales":{"10m":45.2},"vent_direction":{"10m":234},"iso_zero":1804,"risque_neige":"non","cape":0,"nebulosite":{"haute":0,"moyenne":67,"basse":64,"totale":91}}}
"""
        let data = Data(jsonString.utf8)
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let result = WeatherFetchService.shared.testApiResponseJsonParsing(json: json)
                switch result {
                case .failure(let err):
                    XCTAssertEqual(nil, err.localizedDescription)
                case .success(let res):
                    XCTAssertEqual(res.count, 3)
                    XCTAssertEqual(res[0].timestamp, "2019-10-18 05:00:00")
                    XCTAssertEqual(res[1].timestamp, "2019-10-18 08:00:00")
                    XCTAssertEqual(res[2].timestamp, "2019-10-18 11:00:00")
                    XCTAssertEqual(res[0].averageWindSpeed, "22.7")
                    XCTAssertEqual(res[1].averageWindSpeed, "30.7")
                    XCTAssertEqual(res[2].averageWindSpeed, "25.6")
                }
            }
        } catch  {
            XCTAssert(false)
        }
         
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
