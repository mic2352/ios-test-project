//
//  Test_Weather_AppUITests.swift
//  Test Weather AppUITests
//
//  Created by chenxu on 2019/10/18.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest

class Test_Weather_AppUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testStaticLabelsCorrectness() {
        let app = XCUIApplication()
        app.launch()
        
        let tablesQuery = app.tables
        XCTAssert(tablesQuery.staticTexts["Paris"].exists)
        XCTAssert(tablesQuery.staticTexts["Lyon"].exists)
        XCTAssert(tablesQuery.staticTexts["Marseille"].exists)
        XCTAssert(app.navigationBars["Test_Weather_App.PlaceTableView"].buttons["weather around me >"].exists)
        
    }

//    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
