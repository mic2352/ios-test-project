//
//  AppError.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

enum AppError: Error {
    case apiResponseStatusError(Int)
    case emptyResponseError
    
    case networkCheckInitiationError
    
    case localFileDirectoryError
    case localFileSaveError(String)
    case localFileNotFoundError
    case localFileLoadError(String)
    case dataUnavailableError
    
    case locationRetrieveError(String)
}
