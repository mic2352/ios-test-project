//
//  UIViewExtention.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

extension UIView {
    
    /// pin to superView's all edges
    func pin(to superView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        leadingAnchor.constraint(equalTo: superView.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: superView.trailingAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
    }
    
}
