//
//  Place.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/20.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

struct Place {
    var placeName: String
    var coordinates: String
}
