//
//  HourlyForcast.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

struct WeatherInfo: Codable {
    var placeName: String
    var forcasts: [Weather]
}

class Weather: Codable {
    var timestamp: String?
    var temperature: String?
    var rainLevel: String?
    var humidity: String?
    var averageWindSpeed: String?
}


