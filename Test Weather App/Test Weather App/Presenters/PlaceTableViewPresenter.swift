//
//  PlaceTableViewPresenter.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/20.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import CoreLocation

struct PlaceTableViewData{
    let name: String
}

protocol PlaceTableView: CLLocationManagerDelegate {
    func setPlaces(place: [PlaceTableViewData])
    func presentErrorDialog(_ err: Error, completion: (() -> Void)? )
    func navigateToWeatherTableView(weatherInfo: WeatherInfo)
    func presentAlertForSettings()
    func showActivityIndicatorPopup()
    func dismissActivityIndicatorPopup(postFunc: @escaping ()->Void)
}

class PlaceTableViewPresenter {
    weak private var placeTableView: PlaceTableView?
    
    ///nomally this should come from a data source or service
    var places = [Place(placeName: "Paris", coordinates: "48.8566,2.3522"),
                  Place(placeName: "Lyon", coordinates: "45.7640,4.8357"),
                  Place(placeName: "Marseille", coordinates: "43.2965,5.3698")]
    
//    init(peopleService:PeopleService) {
//        self.peopleService = peopleService
//    }
    let locationManager = CLLocationManager()
    
    func attachView(view: PlaceTableView) {
        placeTableView = view
    }
    
    func detachView() {
        placeTableView = nil
    }
    
    func getPlaces() {
        let mappedPlaces = places.map {
            return PlaceTableViewData(name: "\($0.placeName)")
        }
        self.placeTableView?.setPlaces(place: mappedPlaces)
    }
    
    func placeCellOnClick(place: PlaceTableViewData) {
        var selectedPlace: Place?
        for p in places {
            if p.placeName == place.name {
                selectedPlace = p
            }
        }
        if let selectedPlace = selectedPlace {
            fetchData(place: selectedPlace)
        }
    }
    
    func fetchData(place: Place) {
        if isConnectedToNetwork() {
            fetchDataRemotely(place: place)
        } else {
            fetchDataLocally(place: place)
        }
    }
    
    func locationButtonOnClick() {
        locationManager.delegate = self.placeTableView! as CLLocationManagerDelegate
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer//kCLLocationAccuracyThreeKilometers//kCLLocationAccuracyBest
        checkLocationPermission()
    }
    
    func checkLocationPermission(){
        switch CLLocationManager.authorizationStatus() {

        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
             print("Restricted, device owner must approve")
        case .denied:
             print("Denied, request permission from settings")
             self.placeTableView?.presentAlertForSettings()
        case .authorizedAlways:
             print("Authorized always, proceed")
        case .authorizedWhenInUse:
             print("Authorized when in use, proceed")
            locationManager.requestLocation()
             self.placeTableView?.showActivityIndicatorPopup()
        @unknown default:
             print("unknown default")
        }
    }
    
    func requestLocation(){
        locationManager.requestLocation()
    }
    
    func onRetrieveLocation(coordinates: CLLocationCoordinate2D) {
        self.placeTableView?.dismissActivityIndicatorPopup() {
            let coordinatesApiInput = "\(coordinates.latitude),\(coordinates.longitude)"
            print(coordinatesApiInput)
            let placeAroundUser = Place(placeName: "Place Around Me", coordinates: coordinatesApiInput)
            self.fetchData(place: placeAroundUser)
        }
        
    }
    
    func onLocationError(){
        self.placeTableView?.dismissActivityIndicatorPopup(){
            self.placeTableView?.presentErrorDialog(AppError.locationRetrieveError("failed to get current location. Result from last time will be presented")){
                self.fetchDataLocally(place: Place(placeName: "Place Around Me", coordinates: ""))
            }
        }
    }
    
    fileprivate func fetchDataRemotely(place: Place) {
        WeatherFetchService.shared.fetchWeathers(place: place) { (res) in
            switch res {
            case .success(let apiResponse):
                let weatherInfo = WeatherInfo(placeName: place.placeName, forcasts: apiResponse)
                do {
                    try LocalFileService.shared.save(weatherInfo, with: weatherInfo.placeName)
                } catch let err{
                    self.placeTableView?.presentErrorDialog(err,completion: nil)
                    return
                }
                
                self.placeTableView?.navigateToWeatherTableView(weatherInfo: weatherInfo)
            case .failure(let err):
                self.placeTableView?.presentErrorDialog(err, completion: nil)
            }
        }
    }
    
    fileprivate func fetchDataLocally(place: Place) {
        var weatherInfo: WeatherInfo
        do {
            weatherInfo = try LocalFileService.shared.load(place.placeName, with: WeatherInfo.self)
        } catch let err {
            self.placeTableView?.presentErrorDialog(err, completion: nil)
            return
        }
        self.placeTableView?.navigateToWeatherTableView(weatherInfo: weatherInfo)
    }
}

//extension PlaceTableViewPresenter: CLLocationManagerDelegate {
//    
//    ///***** Location delegate methods *****//
//    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        //print("locations = \(locValue.latitude) \(locValue.longitude)")
//        
//    }
//    
//    internal func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse {
//            locationManager.requestLocation()
//        }
//    }
//    
//    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("error:: \(error.localizedDescription)")
//    }
//
//}


