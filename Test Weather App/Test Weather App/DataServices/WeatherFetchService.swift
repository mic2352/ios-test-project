//
//  WeatherFetchService.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class WeatherFetchService: NSObject {
    static let shared = WeatherFetchService()
    
    func fetchWeathers(place: Place, completion: @escaping (Result<[Weather], Error>) -> ()) {
        let urlString = "https://www.infoclimat.fr/public-api/gfs/json?_ll=\(place.coordinates)&_auth=CRMHEAZ4VHZTflptB3EGL1U9UmdeKFB3BnoBYlo%2FB3pUPwJjVTUBZ1Y4VCkPIAcxAC0CYV1mCDhQO1cvWihePwljB2sGbVQzUzxaPwcoBi1Ve1IzXn5QdwZkAW9aNAd6VDICZ1UoAWFWMVQoDz4HMwAsAn1dYwg1UDRXMVozXj0JbwdmBmxUNFMjWicHMgZmVTdSO14zUD4GZAFuWj4HMVQ1AmRVYwFgVidUNA89BzIAMAJrXWYIMVAyVy9aKF5ECRkHfgYlVHRTaVp%2BByoGZ1U4UmY%3D&_c=a706497355408ef31880b6066587227d"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(.failure(err))
                return
            }
            
            guard let data = data else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    DispatchQueue.main.async {
                        completion(self.apiResponseJsonParsing(json: json))
                    }
                }
            } catch let error {
                completion(.failure(error))
            }
            
            
            /// Actually the first layer of JSON cannot be decoded by JSONDecoder() 

        }.resume()
    }
    
    fileprivate func apiResponseJsonParsing(json: [String: Any]) -> Result<[Weather], Error> {
        
        var forcasts = [Weather]()
        
        for (key, value) in json {
            switch key {
            case "request_state":
                if let state = value as? Int {
                    if state != 200 {
                        return .failure(AppError.apiResponseStatusError(state))
                    }
                }
            case "request_key":
                continue
            case "message":
                continue
            case "model_run":
                continue
            case "source":
                continue
            default:
                /* real hourly weather forcast must be here */
                if let hourlyForcast = hourlyForcastParsing(time: key, body: value) {
                    forcasts.append(hourlyForcast)
                }
            }
        }
        
        if forcasts.count == 0 {
            return .failure(AppError.emptyResponseError)
        } else {
            return .success(forcasts.sorted(by: { $0.timestamp! < $1.timestamp! }))
        }
    }
    
    fileprivate func hourlyForcastParsing(time: String, body: Any) -> Weather? {
        
        let hourlyForcast = Weather()
        
        if let json = body as? [String: Any] {
            for (key, value) in json {
                switch key {
                case "temperature":
                    guard let temp = value as? [String: Double] else { continue }
                    guard let tempString = temp["2m"] else { continue }
                    hourlyForcast.temperature = String(tempString)
                    
                case "pluie":
                    guard let temp = value as? Double else { continue }
                    hourlyForcast.rainLevel = String(temp)
                    
                case "humidite":
                    guard let temp = value as? [String: Double] else { continue }
                    guard let tempString = temp["2m"] else { continue }
                    hourlyForcast.humidity = String(tempString)
                
                case "vent_moyen":
                    guard let temp = value as? [String: Double] else { continue }
                    guard let tempString = temp["10m"] else { continue }
                    hourlyForcast.averageWindSpeed = String(tempString)
                    
                default:
                    continue
                }
            }
            hourlyForcast.timestamp = time
            return hourlyForcast
        }
        return nil
    }
}


#if DEBUG
extension WeatherFetchService {
    internal func testApiResponseJsonParsing(json: [String: Any]) -> Result<[Weather], Error> {
        return apiResponseJsonParsing(json: json)
    }
}
#endif
