//
//  LocalFileService.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/20.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class LocalFileService: NSObject {
    static let shared = LocalFileService()
    
    /// get Document Directory
    fileprivate func getDocumentDirectory () throws -> URL  {
        if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            return url
        }else{
            throw AppError.localFileDirectoryError
        }
    }
    
    /// Save any kind of codable objects
    func save <T:Encodable> (_ object:T, with fileName:String) throws {
        let url = try getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)

        }catch{
            throw AppError.localFileSaveError(error.localizedDescription)
        }
        
    }
    
    
    /// Load any kind of codable objects
    func load <T:Decodable> (_ fileName:String, with type:T.Type) throws -> T {
        let url = try getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        if !FileManager.default.fileExists(atPath: url.path) {
            throw AppError.localFileNotFoundError
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            do {
                let model = try JSONDecoder().decode(type, from: data)
                return model
            }catch{
                throw AppError.localFileLoadError(error.localizedDescription)
            }
            
        }else{
            throw AppError.dataUnavailableError
        }
        
    }
    
}
