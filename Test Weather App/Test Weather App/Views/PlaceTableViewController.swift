//
//  ViewController.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/18.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import CoreLocation

class PlaceTableViewController: UIViewController {
    
    var tableView = UITableView()
    let presenter = PlaceTableViewPresenter()
    var placesToDisplay = [PlaceTableViewData]()
    
    @objc let  activityIndicatorAlert = UIAlertController(title: "Retrieving location, it takes a while...", message: nil, preferredStyle: .alert)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationItem()
        configureTableView()
        
        presenter.attachView(view: self)
        presenter.getPlaces()
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 100
        tableView.register(PlaceTableViewCell.self, forCellReuseIdentifier: "placeTableCell")
        tableView.pin(to: view)
    }
    
    func configureNavigationItem(){
        let appTitle = UILabel()
        appTitle.text = "WeatherApp"
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: appTitle)
        
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 20, width: 8, height: 16)
        button.addTarget(self, action: #selector(locationButtonOnClick), for: .touchUpInside)
        button.setTitle("weather around me >", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.sizeToFit()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func locationButtonOnClick(){
        presenter.locationButtonOnClick()
    }
}

extension PlaceTableViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeTableCell", for: indexPath) as! PlaceTableViewCell
        cell.placeNameLabel.text = placesToDisplay[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        presenter.placeCellOnClick(place: placesToDisplay[indexPath.row])
        
    }
}

extension PlaceTableViewController: PlaceTableView {
    func setPlaces(place: [PlaceTableViewData]) {
        placesToDisplay = place
        tableView.reloadData()
    }
    
    func presentErrorDialog(_ err: Error, completion: (() -> Void)? ){
        let alert = UIAlertController(title: "Error", message: "This happens : \(err)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
            if let cb = completion { cb() }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func navigateToWeatherTableView(weatherInfo: WeatherInfo){
        let vc = WeatherTableViewController()
        vc.weatherInfo = weatherInfo
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func presentAlertForSettings(){
        let alert = UIAlertController(title: "Location usage not allowed", message: "Please grant location permission to find weather around you", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { action in
            switch action.style{
            case .default:
                UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            case .cancel: return
            case .destructive: return
            @unknown default: return
            }}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showActivityIndicatorPopup(){
        self.present(activityIndicatorAlert, animated: true)
    }
    
    func dismissActivityIndicatorPopup(postFunc: @escaping ()->Void){
        activityIndicatorAlert.dismiss(animated: true, completion: postFunc)
    }
}

extension PlaceTableViewController: CLLocationManagerDelegate {
    
    ///***** Location delegate methods *****//
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            presenter.onRetrieveLocation(coordinates: locValue)
        }
        
        
    }
    
    internal func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            presenter.requestLocation()
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        presenter.onLocationError()
    }

}
