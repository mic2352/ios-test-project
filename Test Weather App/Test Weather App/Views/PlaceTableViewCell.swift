//
//  PlaceTableViewCell.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    var placeNameLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(placeNameLabel)
        
        configurePlaceLabel()
        setLabelConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurePlaceLabel() {
        placeNameLabel.numberOfLines = 0
        placeNameLabel.adjustsFontSizeToFitWidth = true
    }

    func setLabelConstraints() {
        placeNameLabel.translatesAutoresizingMaskIntoConstraints = false
        placeNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        placeNameLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
}
