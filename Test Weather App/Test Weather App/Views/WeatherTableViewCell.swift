//
//  WeatherTableViewCell.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/19.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    var timestampLabel = UILabel()
    var temperatureLabel = UILabel()
    var rainLabel = UILabel()
    var humidityLabel = UILabel()
    var averageWindSpeedLabel = UILabel()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(timestampLabel)
        addSubview(temperatureLabel)
        addSubview(rainLabel)
        addSubview(humidityLabel)
        addSubview(averageWindSpeedLabel)
        
        configureLabels()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureLabels() {
        timestampLabel.numberOfLines = 0
        timestampLabel.adjustsFontSizeToFitWidth = true
        
        temperatureLabel.numberOfLines = 0
        temperatureLabel.adjustsFontSizeToFitWidth = true
        
        rainLabel.numberOfLines = 0
        rainLabel.adjustsFontSizeToFitWidth = true
        
        humidityLabel.numberOfLines = 0
        humidityLabel.adjustsFontSizeToFitWidth = true
        
        averageWindSpeedLabel.numberOfLines = 0
        averageWindSpeedLabel.adjustsFontSizeToFitWidth = true
        
    }

    func setConstraints() {
        
        timestampLabel.translatesAutoresizingMaskIntoConstraints = false
        timestampLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        timestampLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30).isActive = true
        
        temperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        temperatureLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        temperatureLabel.topAnchor.constraint(equalTo: timestampLabel.topAnchor, constant: 30).isActive = true
        
        rainLabel.translatesAutoresizingMaskIntoConstraints = false
        rainLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        rainLabel.topAnchor.constraint(equalTo: temperatureLabel.topAnchor, constant: 30).isActive = true
        
        humidityLabel.translatesAutoresizingMaskIntoConstraints = false
        humidityLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        humidityLabel.topAnchor.constraint(equalTo: rainLabel.topAnchor, constant: 30).isActive = true
        
        averageWindSpeedLabel.translatesAutoresizingMaskIntoConstraints = false
        averageWindSpeedLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        averageWindSpeedLabel.topAnchor.constraint(equalTo: humidityLabel.topAnchor, constant: 30).isActive = true
    }
}
