//
//  WeatherViewController.swift
//  Test Weather App
//
//  Created by chenxu on 2019/10/18.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

///actually this WeatherTableView only get to present views, thus no presenter is created
class WeatherTableViewController: UIViewController {

    var tableView = UITableView()
    
    var weatherInfo: WeatherInfo!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = weatherInfo.placeName
        configureTableView()
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 200
        tableView.register(WeatherTableViewCell.self, forCellReuseIdentifier: "weatherTableCell")
        tableView.pin(to: view)
    }
    
}

extension WeatherTableViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherInfo.forcasts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherTableCell", for: indexPath) as! WeatherTableViewCell

        cell.timestampLabel.text = weatherInfo.forcasts[indexPath.row].timestamp
        cell.temperatureLabel.text = "Temperature:" + (weatherInfo.forcasts[indexPath.row].temperature ?? "") + "K"
        cell.rainLabel.text = "Rainfall:" + (weatherInfo.forcasts[indexPath.row].rainLevel ?? "") + "mm"
        cell.humidityLabel.text = "Humidity:" + (weatherInfo.forcasts[indexPath.row].humidity ?? "") + "%"
        cell.averageWindSpeedLabel.text = "Average Wind Speed:" + (weatherInfo.forcasts[indexPath.row].averageWindSpeed ?? "") + "km/h"
        
        return cell
    }


}
